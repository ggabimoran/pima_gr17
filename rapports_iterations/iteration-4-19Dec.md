# Rapport d'itération  

## Composition de l'équipe 

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Oscar Pothier            |
| **Scrum Master**        | Thomas Su                |

## Bilan de l'itération précédente  
### Évènements 

> *semaine de partiels* 

### Taux de complétion de l'itération  
> *3 terminés / 3 prévues = 100%*

### Liste des User Stories terminées
> *L'utilisateur veut consulter les news les plus récentes concernant ses influenceurs favoris*
> *L'utilisateur veut pouvoir observer un ranking de ses influenceurs*
> *L'utilisatuer peut créér une liste personnelle d'influenceurs favoris*

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> *Toutes les users stories terminées dans la limite de nos capacités de travail (semaine des partiels) et terminaison du projet*

## Confiance 
### Taux de confiance de l'équipe dans l'itération 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *6* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *6* 	|  *0* 	|

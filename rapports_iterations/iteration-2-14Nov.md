# Rapport d'itération  

## Composition de l'équipe 

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Oscar Pothier            |
| **Scrum Master**        | Maxime Song              |

## Bilan de l'itération précédente  
### Évènements 

> *Vacances et partiels de mi-semestre à la rentrée* 

### Taux de complétion de l'itération  
> *4 terminés / 8 prévues = 50%*

### Liste des User Stories terminées
> *L'utilisateur veut qu'il y ait une base de données sur les influenceurs*
> *L'utilisatuer veut accéder à une liste d'influenceurs*
> *L'utilisateur veut qu'il y ait une fiche d'influenceurs (avec wikipedia)*
> *L'utlisateur veut une bonne ui (sous wordpress?)*

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> *Manque d'investissement dû aux partiels de mi-semestre et perte significative de temps en essayant d'obtenir les droits d'accés des api facebook et instagram*

### Actions prises pour la prochaine itération
*Utilisation des API de google, twitter, fiches d'influenceurs customisées*
 
### Axes d'améliorations
> Travail plus assidu

## Prévisions de l'itération suivante  
### Évènements prévus  

> *rien de particulier*

### Titre des User Stories reportées  
> *L'utilisateur peut créer un compte utilisateur*
> *L'utilisateur peut accéder à son compte*
> *L'utilisatuer peut créér une liste personnelle d'influenceurs favoris*
> *L'utilisateur veut une bonne ui*

### Titre des nouvelles User Stories  
> *L'utilisateur veut qu'il y ait une base de données sur les influenceurs*
> *L'utilisateur veut pouvoir retrouver les trends de son emplacement géographique*
> *L'utilisateur veut pouvoir observer des timelines de ses influenceurs favoris*


## Confiance 
### Taux de confiance de l'équipe dans l'itération 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *3* 	|  *3* 	    |  *0* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *3* 	|  *3* 	    |  *0* 	|  *0* 	|

# Rapport d'itération  

## Composition de l'équipe 

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Oscar Pothier            |
| **Scrum Master**        | Gabriel Moran            |

## Bilan de l'itération précédente  
### Évènements 

> *Mise en place d'un site fonctionnel sous Docker, avec la barre de recherche permettant d'accéder à une liste d'influenceurs pour l'instant répertoriée dans une base de données, et affichant les données correspondant aux influenceurs.* 

### Taux de complétion de l'itération  
> *5 terminés / 6 prévues = 83%*

### Liste des User Stories terminées
> *L'utilisateur veut un site web fonctionnelle*
> *L'utilisateur veut qu'il y ait une barre de recherche*
> *L'utilisateur veut que sa barre de recherche soit fonctionnelle*
> *L'utilisateur veut pouvoir cliquer sur les résultats de sa recherche*
> *L'utilisateur veut pouvoir accéder via un site web à la base de données des influenceurs*

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> *Investissement déséquilibré dans le projet, définition des rôles trop vague.*

### Actions prises pour la prochaine itération
*Utilisation des API de facebook, instagram, wikipedia, google, twitter, meilleur ui (passage à wordpress?), fiches d'influenceurs, base de données plus extensive*
 
### Axes d'améliorations
> Meilleur investissement, meilleur communication du scrum master sur la définition des rôles

## Prévisions de l'itération suivante  
### Évènements prévus  

> *Vacances et plusieurs partiels de mi-semestre à la rentrée*

### Titre des User Stories reportées  
> *L'utilisateur veut qu'il y ait une fiche d'influenceurs*

### Titre des nouvelles User Stories  
> *L'utilisateur veut qu'il y ait une base de données sur les influenceurs*
> *L'utilisatuer veut accéder à une liste d'influenceurs*
> *L'utlisateur veut une bonne ui*
> *L'utilisateur veut son site sous wordpress*
> *L'utilisateur peut créer un compte utilisateur*
> *L'utilisateur peut accéder à son compte*
> *L'utilisatuer peut créér une liste personnelle d'influenceurs favoris*

## Confiance 
### Taux de confiance de l'équipe dans l'itération  

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *6* 	    |  *0* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *6* 	    |  *0* 	|  *0* 	|

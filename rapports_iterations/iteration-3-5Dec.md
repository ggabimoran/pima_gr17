# Rapport d'itération  

## Composition de l'équipe 

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Oscar Pothier            |
| **Scrum Master**        | Bihe Dong                |

## Bilan de l'itération précédente  
### Évènements 

> *Rien de particulier* 

### Taux de complétion de l'itération  
> *6 terminés / 7 prévues = 85%*

### Liste des User Stories terminées
> *L'utilisateur veut qu'il y ait une base de données sur les influenceurs*
> *L'utilisateur veut pouvoir retrouver les trends de son emplacement géographique*
> *L'utilisateur veut pouvoir observer des timelines de ses influenceurs favoris*
> *L'utilisateur peut créer un compte utilisateur*
> *L'utilisateur peut accéder à son compte*
> *L'utilisateur veut une bonne ui*

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
> *Bien meilleur investissement collectif, toutes les users stories terminées sauf la liste customisée par encore fonctionnelle*

### Actions prises pour la prochaine itération
*récolte de données via api twitter, liste customisée des influenceurs et ranking des influenceurs*
 
### Axes d'améliorations
> *continuer avec le même rythme*

## Prévisions de l'itération suivante  
### Évènements prévus  

> *semaine de partiels*

### Titre des User Stories reportées 
> *L'utilisatuer peut créér une liste personnelle d'influenceurs favoris*

### Titre des nouvelles User Stories  
> *L'utilisateur veut consulter les news les plus récentes concernant ses influenceurs favoris*
> *L'utilisateur veut pouvoir observer un ranking de ses influenceurs*

## Confiance 
### Taux de confiance de l'équipe dans l'itération 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *6* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *6* 	|  *0* 	|

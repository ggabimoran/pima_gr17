DROP TABLE IF EXISTS "user.sql" CASCADE;
DROP TABLE IF EXISTS "influencer.sql" CASCADE;

\i data/user.sql;
\i data/influencer.sql;
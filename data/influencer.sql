create table if not exists "influencers"  (id serial primary key, name varchar(30) not null, twitter_username varchar(30) not null, nb_followers int);

insert into influencers(name,twitter_username) values ('Barack Obama','@BarackObama');
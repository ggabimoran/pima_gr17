# PIMA_Gr17

### Composition de l'équipe

| Nom          | Prénom      | Email                         |
| -------------|-------------|-------------------------------|
| Moran        | Gabriel
| Song         | Maxime	
| Pothier      | Oscar
| Dong         | Bihe
| Su           | Thomas
| Wu           | Haowen

### Sujet : Know Your Influencer

### Adresse du backlog : https://gitlab.com/ggabimoran/pima_gr17/tree/master


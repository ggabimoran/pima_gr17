<?php
function head($addInHead,$relative_path){
    echo '
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Know your influencer</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="'.$relative_path.'img/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->'.
    $addInHead.'
    <link rel="stylesheet" href="'.$relative_path.'css/bootstrap.min.css">
    <link rel="stylesheet" href="'.$relative_path.'css/owl.carousel.min.css">
    <link rel="stylesheet" href="'.$relative_path.'css/magnific-popup.css">
    <link rel="stylesheet" href="'.$relative_path.'css/font-awesome.min.css">
    <link rel="stylesheet" href="'.$relative_path.'css/themify-icons.css">
    <link rel="stylesheet" href="'.$relative_path.'css/nice-select.css">
    <link rel="stylesheet" href="'.$relative_path.'css/flaticon.css">
    <link rel="stylesheet" href="'.$relative_path.'css/gijgo.css">
    <link rel="stylesheet" href="'.$relative_path.'css/animate.css">
    <link rel="stylesheet" href="'.$relative_path.'css/slicknav.css">
    <link rel="stylesheet" href="'.$relative_path.'css/style.css">
    <!-- <link rel="stylesheet" href="'.$relative_path.'css/responsive.css"> -->
</head>'."\n";
}

function header_area(){
    echo '
<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                    <div class="row align-items-center no-gutters">
                        <div class="col-xl-9 col-lg-9">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="active" href="index.html">home</a></li>
                                        <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="blog.html">blog</a></li>
                                                <li><a href="single-blog.html">single-blog</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->'."\n";
}

function slider($form){
    echo '
<!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider d-flex align-items-center justify-content-center slider_bg_1 overlay2">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-9">
                        <div class="slider_text text-center">
                            <p>The Best Personality Follower App</p>
                            <h3>Know Your Influencer</h3>
                            <div class="find_dowmain">'.$form.
                                
                            '</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->'."\n";
}


function faq(){
    echo '
<!-- faq_area_start -->
    <div class="faq_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="accordion_heading">
                        <h3>Frequently Ask Question</h3>
                    </div>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="flaticon-info"></i> Is WordPress hosting worth it?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                data-parent="#accordion">
                                <div class="card-body">
                                    Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                    let god moving. Moving in fourth air night bring upon
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <i class="flaticon-info"></i> What are the advantages <span>of WordPress hosting
                                            over shared?</span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion"
                                style="">
                                <div class="card-body">
                                    Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                    let god moving. Moving in fourth air night bring upon
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                        <i class="flaticon-info"></i> Will you transfer my site?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                data-parent="#accordion">
                                <div class="card-body">
                                    Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                    let god moving. Moving in fourth air night bring upon
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading_4">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapse_4" aria-expanded="false" aria-controls="collapse_4">
                                        <i class="flaticon-info"></i> Why should I hosting with ?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse_4" class="collapse" aria-labelledby="heading_4" data-parent="#accordion">
                                <div class="card-body">
                                    Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                    let god moving. Moving in fourth air night bring upon
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading_5">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapse_5" aria-expanded="false" aria-controls="collapse_5">
                                        <i class="flaticon-info"></i> How do I get started <span>with Shared
                                            Hosting?</span>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse_5" class="collapse" aria-labelledby="heading_5" data-parent="#accordion">
                                <div class="card-body">
                                    Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                    let god moving. Moving in fourth air night bring upon
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- faq_area_end -->'."\n";
}

function latest_new_area(){
    echo '
<!-- latest_new_area_start -->
    <div class="latest_new_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-100">
                        <h3>
                            Latest News
                        </h3>
                        <p>Your domain control panel is designed for ease-of-use and <br>
                            allows for all aspects of your domains.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_news">
                        <div class="thumb">
                            <a href="#">
                                <img src="img/news/1.png" alt="">
                            </a>
                        </div>
                        <div class="news_content">
                            <div class="news_meta">
                                <a href="#">12 Jun, 2019 in <span>Hosting tips</span> </a>
                            </div>
                            <h3><a href="#">Commitment to dedicated
                                    Support</a></h3>
                            <p class="news_info">Firmament morning sixth subdue darkness</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_news">
                        <div class="thumb">
                            <a href="#">
                                <img src="img/news/2.png" alt="">
                            </a>
                        </div>
                        <div class="news_content">
                            <div class="news_meta">
                                <a href="#">12 Jun, 2019 in <span>Hosting tips</span> </a>
                            </div>
                            <h3><a href="#">Commitment to dedicated
                                    Support</a></h3>
                            <p class="news_info">Firmament morning sixth subdue darkness</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_news">
                        <div class="thumb">
                            <a href="#">
                                <img src="img/news/3.png" alt="">
                            </a>
                        </div>
                        <div class="news_content">
                            <div class="news_meta">
                                <a href="#">12 Jun, 2019 in <span>Hosting tips</span> </a>
                            </div>
                            <h3><a href="#">Commitment to dedicated
                                    Support</a></h3>
                            <p class="news_info">Firmament morning sixth subdue darkness</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- latest_new_area_end -->'."\n";
}

function lets_launch(){
    echo '
<!-- lets_launch_start -->
    <div class="lets_launch launch_bg_1 overlay2">
        <div class="launch_text text-center">
            <h3>Let’s Launch your Website Now</h3>
            <p>Our set he for firmament morning sixth subdue darkness creeping gathered divide our let god moving. <br>
                Moving in fourth air night bring upon you’re it beast.</p>
            <div class="chat">
                <a class="boxed_btn_green" href="#">
                    <i class="flaticon-chat"></i>
                    <span>Live Chat</span>
                </a>
                <a class="boxed_btn_green2" href="#">
                    <span>get start now</span>
                </a>
            </div>
        </div>
    </div>
    <!-- lets_launch_end -->'."\n";
}

function test_form(){
    echo '
<!-- form itself end-->
    <form id="test-form" class="white-popup-block mfp-hide">
        <div class="popup_box ">
            <div class="popup_inner">
                <h3>Sign in</h3>
                <form action="#">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <input type="email" placeholder="Enter email">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <input type="password" placeholder="Password">
                        </div>
                        <div class="col-xl-12">
                            <button type="submit" class="boxed_btn_green">Sign in</button>
                        </div>
                    </div>
                </form>
                <p class="doen_have_acc">Don’t have an account? <a class="dont-hav-acc" href="#test-form2">Sign Up</a>
                </p>
            </div>
        </div>
    </form>
    <!-- form itself end -->'."\n";
}

function test_form2(){
    echo '<!-- form itself end-->
    <form id="test-form2" class="white-popup-block mfp-hide">
        <div class="popup_box ">
            <div class="popup_inner">
                <h3>Resistration</h3>
                <form action="#">
                    <div class="row">
                        <div class="col-xl-12 col-md-12">
                            <input type="email" placeholder="Enter email">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <input type="password" placeholder="Password">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <input type="Password" placeholder="Confirm password">
                        </div>
                        <div class="col-xl-12">
                            <button type="submit" class="boxed_btn_green">Sign Up</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
    <!-- form itself end -->'."\n";
}

function insert_js(){
    echo '
<!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>

    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>

    <script src="js/main.js"></script>'."\n";
}

function page_end(){
    echo '
</body>

</html>'."\n";
}
?>





























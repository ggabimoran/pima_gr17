<?php 
require "twitteroauth/autoload.php";
$access_token='1187012801748967426-jhJcjZDl3JyTdjiUIRKVWJOutkkyVx';
$access_token_secret='CLQIZdxcPQHy9kM5M8QQs4tIVM7l0ndDG29GNSjgyb8b3';
$CONSUMER_KEY = 'cne5TBIyfDsN0fyXU3aodbvZi';
$CONSUMER_SECRET = 'BWF1W2lZJhnf5sdjyUkor75JwLVQuoyHClGrhmkp5ZfEHBeobh';

use Abraham\TwitterOAuth\TwitterOAuth;

include 'Vue.php';


abstract class Content{
    const None = 0;
    const Timeline = 1;
    const Retweets = 2;
    const Lists = 3;
    const Trends = 4;
    const UserSearch = 5;
    const UsersSet = 6;
}

function getUser($connection, $screen_name){
    $result = $connection->get("users/show",["screen_name" => "$screen_name"]);
    $arr_result = json_decode(json_encode($result),true);
    return $arr_result;   
}

function getUserTimeline($connection, $screen_name, $only_rts){
    $tweets = $connection->get("statuses/user_timeline",["screen_name" => "$screen_name","exclude_replies" => true,"count"=>200,"include_rts" => $only_rts]);
    $arr_tweets = json_decode(json_encode($tweets),true);
    if ($only_rts){
        $res = array();
        foreach($arr_tweets as $tweet){
            if (substr($tweet["text"],0,2) == 'RT'){
                array_push($res,$tweet);
            }
        }
        return $res;
    }
    else{
        return $arr_tweets; 
    }      
}

function getLists($connection, $screen_name){
    $result = $connection->get("lists/list",["screen_name" => "$screen_name"]);
    $arr_result = json_decode(json_encode($result),true);
    return $arr_result;
}

function getListTimeline($connection,$list_id,$include_rts){
    $result = $connection->get("lists/statuses",["list_id"=>$list_id,"count"=>200,"include_rts"=>$include_rts]);
    $arr_result = json_decode(json_encode($result),true);
    return $arr_result;
}

function getTrends($connection,$id){
    $result = $connection->get("trends/place",["id"=>$id]);
    $temp_arr_result = json_decode(json_encode($result),true);
    if ($temp_arr_result){
      $arr_result = $temp_arr_result[0];
      return $arr_result;
    }
    else{
        return null;
    }
}

function showTrends($arr_trends){
    $arr_locations = $arr_trends["locations"];
    $l_location = $arr_locations[0];
    $l_trends = $arr_trends["trends"];
    echo '<br>Location: '.$l_location["name"].'<br>WOEID: '.$l_location["woeid"].'<br><br>'."\n";
    foreach ($l_trends as $trend){
        echo '<a href='.$trend["url"].'>'.$trend["name"].'</a><br>Tweet volume: '.$trend["tweet_volume"].'<br><br>'."\n";
    }
}

//$user is array returned by getUser
function showUserInformation($user){
    echo "Name: " . $user["name"] .'<br>' . "\n";
    echo "Location: " . $user["location"] . '<br>' . "\n";
    echo "Number of followers: " . $user["followers_count"] . '<br>' . "\n";
    echo "<img src=" . $user['profile_image_url'] . " alt='Profile Pic'>" . "<br>" . "\n";
    echo '<br>' . "\n";
}

//$arr_tweets is array returned by getTwitterUserTimeline
// and $user array returned by getUser
function showTimeline($arr_tweets){
    foreach ($arr_tweets as $tweet){
        $user = $tweet["user"];
        echo '<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">';
        echo $tweet["text"] . " ";
        echo '</p>';
        echo '&mdash;'.$user["name"].'(@'.$user["screen_name"].')';
        $date = substr($tweet["created_at"],4,6) . ', ' . substr($tweet["created_at"],-4);
        echo '<a href="https://twitter.com/'.$user["screen_name"].'/status/'.$tweet["id"].'?ref_src=twsrc%5Etfw">'.$date.'</a>';
        echo '</blockquote>';
        echo '<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>';
    }
}

function showUserLists($arr_lists){
    global $screen_name;
    echo '<form action="index.php" method="post"><select name="listid">'."\n";
    foreach ($arr_lists as $list){
        echo '<option value='.$list['id'].'>'.$list['name'].'</option>'."\n";
    }
    echo '</select>
<input type="hidden" name="content" value='.Content::Lists.'>
<input type="hidden" name="screen_name" value='.$screen_name.'>
Include Retweets : <input type="checkbox" name="include_rts" value="yes">
<input type="submit" value="Submit">
</form>
' . "\n";
}

function showTrendsSearchBar(){
    global $content;
    echo '<form action="index.php" method="post">
Search Trends Near A Location (WOEID): <input type="text" name="woeid">
<input type="hidden" name="content" value='.$content.'>
<input type="submit" value="Submit">
</form>
' . "\n" . "(try 2459115 for New York)" . "\n";
}

function showUserSearchBar(){
    echo '<form action="index.php" method="post">
Influencer Name : <input type="text" name="screen_name">
<select name="content">
  <option value='.Content::Timeline.'>Timeline</option>
  <option value='.Content::Retweets.'>Retweets</option>
  <option value='.Content::Lists.'>Lists</option>
</select>
<input type="submit" value="Submit">
</form>
' . "\n";
}

function selectionGeneralContent(){
    echo '<form action="index.php" method="post">
<select name="content">
  <option value='.Content::UserSearch.'>Influencer</option>
  <option value='.Content::UsersSet.'>Customized Influencer Set</option>
  <option value='.Content::Trends.'>Trends Per Location</option>
</select>
<input type="submit" value="Submit">
</form>
' . "\n";
}

function twitterhead(){
head('<link rel="stylesheet" type="text/css" href="twitterapi.css"><meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.dropbtn {
  background-color: #3498DB;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}
        
.dropbtn:hover, .dropbtn:focus {
  background-color: #2980B9;
}
        
.dropdown {
  position: relative;
  display: inline-block;
}
        
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  overflow: auto;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}
        
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}
        
.dropdown a:hover {background-color: #ddd;}
        
.show {display: block;}
</style>','../');

}

if (isset($_POST['screen_name'])) $screen_name = $_POST['screen_name'];
else $screen_name = null;
if (isset($_POST['content'])) $content = $_POST['content'];
else $content = Content::None;

$connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token, $access_token_secret);
if($connection){
    $auth_content = $connection->get("account/verify_credentials");
    if ($connection->getLastHttpCode() == 200) {
        $CONNECTED=true;
    }
    else{
        echo "Credentials not verified" . "\n";
    }
}
else{
    echo "Connection failed" . "\n";
}

twitterhead();
header_area();
echo '<br><br><br>'."\n";//line indent
selectionGeneralContent();
//var_dump($content); for debugging
if ($connection && ($content == Content::UserSearch || $content == Content::Timeline || $content == Content::Lists || $content == Content::Retweets)){
    showUserSearchBar();
    if ($screen_name){
        $user = getUser($connection, "$screen_name");
        showUserInformation($user);
        switch ($content){
            case Content::Timeline :
                $userTimeline = getUserTimeline($connection, $user["screen_name"],false);
                showTimeline($userTimeline);
                break;
            case Content::Retweets :
                $userRetweets = getUserTimeline($connection, $user["screen_name"],true);
                showTimeline($userRetweets);
                break;
            case Content::Lists :
                $lists = getLists($connection, "$screen_name");
                showUserLists($lists);
                if (isset($_POST['listid'])) $listid = $_POST['listid'];
                else $listid = -1;
                if ($listid != -1){
                    $include_rts = (isset($_POST['include_rts']) && $_POST['include_rts']=="yes");
                    $list_timeline = getListTimeline($connection,$listid,$include_rts);
                    showTimeline($list_timeline);
                }
                break;
        }
    }
}

elseif ($connection && $content == Content::Trends){
    if (isset($_POST['woeid'])) $woeid = $_POST['woeid'];
    else $woeid = -1;
    showTrendsSearchBar();
    if ($woeid != -1){
        $trends = getTrends($connection,$woeid);
        if ($trends){
            showTrends($trends);
        }
    }
}

elseif ($connection && $content == Content::UsersSet){
    //to do
    
}

echo '<div><a href="/homepage.php">Go back</a></div>';

insert_js();
page_end();
?>




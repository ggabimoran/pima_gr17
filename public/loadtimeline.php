<?php
session_start();
require('twitterapi/twitteroauth/autoload.php');
require_once('config/dbconfig.php');
require_once('config/twitterconfig.php');
use Abraham\TwitterOAuth\TwitterOAuth;
include('Vue.php');

function getUserTimeline($connection, $screen_name, $only_rts){
    $tweets = $connection->get("statuses/user_timeline",["screen_name" => "$screen_name","exclude_replies" => true,"count"=>5,"include_rts" => $only_rts]);
    $arr_tweets = json_decode(json_encode($tweets),true);
    if ($only_rts){
        $res = array();
        foreach($arr_tweets as $tweet){
            if (substr($tweet["text"],0,2) == 'RT'){
                array_push($res,$tweet);
            }
        }
        return $res;
    }
    else{
        return $arr_tweets; 
    }      
}

function showTimeline($arr_tweets){
    foreach ($arr_tweets as $tweet){
        $user = $tweet["user"];
        echo '<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">';
        echo $tweet["text"] . " ";
        echo '</p>';
        echo '&mdash;'.$user["name"].'(@'.$user["screen_name"].')';
        $date = substr($tweet["created_at"],4,6) . ', ' . substr($tweet["created_at"],-4);
        echo '<a href="https://twitter.com/'.$user["screen_name"].'/status/'.$tweet["id"].'?ref_src=twsrc%5Etfw">'.$date.'</a>';
        echo '</blockquote>';
        echo '<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>';
    }
}

$twitter_connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token, $access_token_secret);
$query = "select name, twitter_username from influencers";
$query_run = $connection->query($query);
$result = $query_run->fetchAll();

head("Twitter timeline");
foreach($result as $row)
{
    $screen_name = $row['twitter_username'];
    $userTimeline = getUserTimeline($twitter_connection,$screen_name,false);
    showTimeline($userTimeline);
}
echo '<a href="homepage.php">Back</a>';
foot();
?>
<?php

function head($title){
    echo '
    <!DOCTYPE html>
    <html>

    <head>
        <title>'.$title.'</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body style="background-color:#bdc3c7">'."\n";
}

function login_form(){
    echo '
    <div id="main-wrapper">
    <center><h2>Login Form</h2></center>
            <div class="imgcontainer">
                <img src="imgs/avatar.png" alt="Avatar" class="avatar">
            </div>
        <form action="index.php" method="post">
            <div class="inner_container">
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>
                <label><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" required>
                <button class="login_button" name="login" type="submit">Login</button>
                <a href="register.php"><button type="button" class="register_btn">Register</button></a>
            </div>
        </form>
    </div>'."\n";
}

function homepage(){
    echo '
    <div id="main-wrapper">
        <center><h2>Home Page</h2></center>
        <center><h3>Welcome '.$_SESSION['username'].'</h3></center>
        
        <form action="index.php" method="post">
            <div class="imgcontainer">
                <img src="imgs/avatar.png" alt="Avatar" class="avatar">
            </div>
            <div class="inner_container">
                <button class="logout_button" type="submit">Log Out</button>    
            </div>
        </form>
    </div>';
}

function influencer_list($tabl){
    echo ' 
    <div id="main-wrapper" class="row">
        <center class="col-12"><h2>List of influencers</h2></center>

        <table class="col-12">
            <tr>
                <th>Name</th>
                <th>Twitter Username</td>
            </tr>';
    foreach ($tabl as $row) {
        $name = $row['name'];
        $twitter_username = $row['twitter_username'];

        echo '
        <tr>
            <td>'.$name.'</td>
            <td>'.$twitter_username.'</td>
        </tr>';
    }
    echo '
        <form action="loadprofil.php" method="post">
            <button name="profil_influencer" type="submit">Get profils</button>
        </form>
        <form action="loadtimeline.php" method="post">
            <button name="timeline_influencer" type="submit">Get timelines</button>
        </form>        
        <form action="loadinfluencerranking.php" method="post">
            <button name="ranking_influencer" type="submit">Get influencers ranking</button>
        </form>';

    echo '
        </table>
        <center><h2>Influencer Form</h2></center>
        <form action="homepage.php" method="post">
            <div class="inner_container">
                <label><b>Name</b></label>
                <input type="text" placeholder="Enter Name" name="name" required>
                <label><b>Twitter Username</b></label>
                <input type="text" placeholder="Enter Twitter Username" name="twitter_username" required>
                <button name="add_influencer" class="sign_up_btn" type="submit">Add</button>
                <button name="rmv_influencer" class="sign_up_btn" type="submit">Remove</button>
            </div>
        </form>
    </div>';
}

function signup_form(){
    echo '
    <div id="main-wrapper">
        <center><h2>Sign Up Form</h2></center>
        <form action="register.php" method="post">
            <div class="imgcontainer">
                <img src="imgs/avatar.png" alt="Avatar" class="avatar">
            </div>
            <div class="inner_container">
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" required>
                <label><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" required>
                <label><b>Confirm Password</b></label>
                <input type="password" placeholder="Enter Password" name="cpassword" required>
                <button name="register" class="sign_up_btn" type="submit">Sign Up</button>
                
                <a href="index.php"><button type="button" class="back_btn"><< Back to Login</button></a>
            </div>
        </form>
    </div>';
}

function foot(){
    echo '
    </body>
    </html>';
}
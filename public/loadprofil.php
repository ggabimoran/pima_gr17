<?php
session_start();
require('twitterapi/twitteroauth/autoload.php');
require_once('config/dbconfig.php');
require_once('config/twitterconfig.php');
use Abraham\TwitterOAuth\TwitterOAuth;
include('Vue.php');

function getUser($connection, $screen_name){
    $result = $connection->get("users/show",["screen_name" => "$screen_name"]);
    $arr_result = json_decode(json_encode($result),true);
    return $arr_result;   
}

function showUserInformation($user){
    echo "Name: " . $user["name"] .'<br>' . "\n";
    echo "Location: " . $user["location"] . '<br>' . "\n";
    echo "Description: " . $user["description"] . '<br>' . "\n";
    echo "Number of followers: " . $user["followers_count"] . '<br>' . "\n";
    echo "<img src=" . $user['profile_image_url'] . " alt='Profile Pic'>" . "<br>" . "\n";
    echo '<br>' . "\n";
}

$twitter_connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token, $access_token_secret);
$query = "select name, twitter_username from influencers";
$query_run = $connection->query($query);
$result = $query_run->fetchAll();

head("Twitter profil");
foreach($result as $row)
{
    $screen_name = $row['twitter_username'];
    $user = getUser($twitter_connection,"$screen_name");
    showUserInformation($user);
}
echo '<a href="homepage.php">Back</a>';
foot();
?>
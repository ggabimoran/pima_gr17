<?php
session_start();
require('twitterapi/twitteroauth/autoload.php');
require_once('config/dbconfig.php');
require_once('config/twitterconfig.php');
use Abraham\TwitterOAuth\TwitterOAuth;
include('Vue.php');

function getUser($connection, $screen_name){
    $result = $connection->get("users/show",["screen_name" => "$screen_name"]);
    $arr_result = json_decode(json_encode($result),true);
    return $arr_result;   
}

$twitter_connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token, $access_token_secret);
$query = "select name, twitter_username from influencers";
$query_run = $connection->query($query);
$result = $query_run->fetchAll();

head("Influencers ranking");
foreach($result as $row)
{
    $name = $row['name'];
    $screen_name = $row['twitter_username'];
    $user = getUser($twitter_connection,"$screen_name");
    $followers = $user['followers_count'];
    $query = "delete from influencers where name='$name' and twitter_username='$screen_name'";
    $query_run = $connection->query($query);
    $query = "insert into influencers(name,twitter_username,nb_followers) values ('$name','$screen_name','$followers')";
    $query_run = $connection->query($query);
}
$query = "select name, twitter_username from influencers order by nb_followers desc";
$query_run = $connection->query($query);
$result = $query_run->fetchAll();
$rank = 0;
foreach ($result as $row) 
{
    $rank = $rank + 1;
    $screen_name = $row['twitter_username'];
    $user = getUser($twitter_connection,"$screen_name");
    echo "Rank: '$rank'" . "<br>";
    echo "Name: " . $user["name"] .'<br>' . "\n";
    echo "Number of followers: " . $user["followers_count"] . '<br>' . "\n";
    echo '<br>' . "\n";
}
echo '<a href="homepage.php">Back</a>';
foot();
?>